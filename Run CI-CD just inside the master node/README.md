# Build a All-in-one Jenkins Docker Image
In my case, I use JDK11 and Maven doing my Java projects. All I can find in jenkins/jenkins is an image [jenkins/jenkins:lts-jdk11](https://hub.docker.com/layers/jenkins/jenkins/lts-jdk11/images/sha256-dfe766e539dc2f47600229c7ed5bb7950b34d2c61f1ae240ef9393cbc0df6e68?context=explore). The image has Jenkins and JDK11, but missing maven. So I have to install maven in this image and here are the steps:

## 1. Prepare the Dockerfile
Let create a file named 'Dockerfile' (a default name) or another name whatever you want in a folder. There is the content:
```
FROM jenkins/jenkins:lts-jdk11

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/var/jenkins_home"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

USER root

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

USER jenkins
```
* First I use the official image 'jenkins/jenkins:lts-jdk11' as a base.
* Second the rest of lines are mainly copied from [another Dockerfile from Maven official](https://github.com/carlossg/docker-maven/blob/master/openjdk-11/Dockerfile). There are the modifications:
  * Argument USER_HOME_DIR changes to JENKINS_HOME (i.e. /var/jenkins_home)
  * Switch to root account to install 
  * After installation completed, switch back to jenkins account
## 2 Build the Image
Run the following commands
~~~
// change the path to the Dockerfile location
cd /path/of/Dockerfile
docker build -t ambrose/jenkins-allinone:jdk11-mvn3 .
// where docker name is 'ambrose/jenkins-allinone' and tag name is 'jdk11-mvn3'

//OR if use another name, then please specify the file name with -f option
docker build -f /path/your_dockerfile_name -t [your_docker_name]:[your_tag].
~~~
If the build is success, you can verify the new image by running 'docker image ls'.
~~~
REPOSITORY                 TAG                 IMAGE ID            CREATED             SIZE
ambrose/jenkins-allinone   jdk11-mvn3          b04e384b75dc        About an hour ago   787MB
~~~
## 3 Further Configuration
Now Maven is installed and the default repository is set in /var/jenkins_home/.m2. There are two options for Maven setting.
* Create a corresponding sub-folder .m2 in the home volume (i.e. jenkins/data/.m2). Remember to change the ownership or grant read/write rights if necessary. Then copy your maven's settings.xml there if you have.
* Add an extra volume mapping in the start-up script or the docker-compose file for mounting your local maven .m2 to container's .m2.  
  * For `docker run` command  
  `-v /home/ambrose/.m2:/var/jenkins_home/.m2`
  * For docker-compose file, add the following line under volumes section  
  `- /home/ambrose/.m2:/var/jenkins_home/.m2`
## 4 Create a Job for Test
* Start-up Jenkins
* Do all initial work if it is first time running (e.g. create an admin account, download the plugins)
* Click 'New Item' on the left panel
* Enter a job name, say 'test'
* Select 'Freestyle project'
* Click 'OK' to go to the detail page
* Click 'Source Code Management' tab in the detail page
* Select the 'Git' option
* Input the git URL of your project
* Add Credential
* Input a branch, say '*/master'
* Select 'Build Environment' tab
* Check 'Delete workspace before build starts'
* Select 'Build' tab
* Select 'Maven 3.6.3' in Maven Version
* Input 'clean compile' in Goals
* Click 'Save' and go back to the Project page
* Click 'Build Now' on the left panel to run the job