# Run CI/CD on a remote Docker
## Enable Docker API port
By default, the API port is disabled. How to enable it?
* For Docker Desktop installed in Windows, you just go to the setting and check the option "Expose daemon on tcp://localhost:2375 without TLS".
* In Linux, there are the steps
  * Open the file /lib/systemd/system/docker.service
  * Search for `ExecStart` 
  * Append `-H tcp://0.0.0.0:4243` and save
  * Run the following two command to restart docker
    * `sudo systemctl daemon-reload`
    * `sudo service docker restart`

## Install Docker plugin in Jenkins
