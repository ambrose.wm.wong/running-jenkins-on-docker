# Create an agent in a Pipeline to run CI/CD jobs
There are two ways to create an agent (a container) dynamically in a Pipeline.
* Create from a Docker image
* Create from a Dockerfile
## Build a Docker Image
In fact, this part may not be a necessary if you can find a Docker image all you need on the Docker Hub. Otherwise, you have to build your own image. Similar to [building all-in-one jenkins image section](https://gitlab.com/ambrose.wm.wong/running-jenkins-on-docker/-/tree/master/Run%20CI-CD%20just%20inside%20the%20master%20node), there are the steps:
### Prepare a Dockerfile
  * Find a base image in Docker Hub closed what you need.
  * Add more instructions in order to complete the image exactly you want.
  * Run [`docker build`][docker-build] command to build the final image.

In my case, I need an image having JDK-11 and Maven. There is a lot of images having these two tools on Docker Hub. I choose [Maven offical image](https://hub.docker.com/_/maven), but there is a minor security problem. The image has to run as root account, otherwise I get no permission error. I think it is not a good idea. Also Jenkins will create a container on your docker using your image and run as jenkins account (uid 1000). Therefore jenkins account is used to run all steps defined in Jenkinsfile. For allowing run as jenkins, I modify [the offical Dockerfile](https://github.com/carlossg/docker-maven/blob/26ba49149787c85b9c51222b47c00879b2a0afde/openjdk-11/Dockerfile) and build it again. Here is my Dockerfile:
```
FROM openjdk:11-jdk

ARG MAVEN_VERSION=3.6.3
ARG USER_HOME_DIR="/home/jenkins"
ARG SHA=c35a1803a6e70a126e80b2b3ae33eed961f83ed74d18fcd16909b2d44d7dada3203f1ffe726c17ef8dcca2dcaa9fca676987befeadc9b9f759967a8cb77181c0
ARG BASE_URL=https://apache.osuosl.org/maven/maven-3/${MAVEN_VERSION}/binaries

RUN groupadd -g 1000 jenkins
RUN useradd -u 1000 -ms /bin/bash -g jenkins jenkins

RUN mkdir -p /usr/share/maven /usr/share/maven/ref \
  && curl -fsSL -o /tmp/apache-maven.tar.gz ${BASE_URL}/apache-maven-${MAVEN_VERSION}-bin.tar.gz \
  && echo "${SHA}  /tmp/apache-maven.tar.gz" | sha512sum -c - \
  && tar -xzf /tmp/apache-maven.tar.gz -C /usr/share/maven --strip-components=1 \
  && rm -f /tmp/apache-maven.tar.gz \
  && ln -s /usr/share/maven/bin/mvn /usr/bin/mvn

ENV MAVEN_HOME /usr/share/maven
ENV MAVEN_CONFIG "$USER_HOME_DIR/.m2"

COPY mvn-entrypoint.sh /usr/local/bin/mvn-entrypoint.sh
COPY settings-docker.xml /usr/share/maven/ref/

ENTRYPOINT ["/usr/local/bin/mvn-entrypoint.sh"]
CMD ["mvn"]

```
* Add a group jenkins with gid 1000
* Add a user jenkins with uid 1000 and add it to jenkins group
* Set MAVEN_CONFIG to jenkins user home

### Build Your Image
Then the [`docker build`][docker-build] command below is run to build my image.  
```
docker build -t ambrose/maven:jdk11-mvn3 .
```
## Create a Container in a Pipeline
A [pipeline](https://www.jenkins.io/doc/pipeline/tour/hello-world/) can be defined by a [Jenkinsfile](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/). In the [agent](https://www.jenkins.io/doc/book/pipeline/syntax/#agent) section of a Jenkinsfile, 'docker' type is declared and with 'image' parameter. Here is my example:
```
pipeline {
    agent {
        docker { 
            image 'amd64/maven:3.6.3-jdk-11'
            args '-v /[path/to]/.m2:/home/jenkins/.m2'
        }
    }
    ...
}
```
* The image parameter set to the image I built in the above section
* Mount my local Maven .m2 to the container's default .m2 folder. Also, a settings.xml is expected in the local Maven .m2 directory.

[docker-build]: <(https://docs.docker.com/engine/reference/commandline/build/> "docker build"

## Let's Complete the Jenkinsfile
Now, in the Jenkinsfile, the pipeline tells Docker to create a container declared in the docker section. Then, in the [stages](https://www.jenkins.io/doc/book/pipeline/syntax/#stages) section, the pipeline instructs  what to do inside container. The stages section must have at least one stage directive for each individual CI/CD process, for example, checkout, build, test and deploy. In a stage, it contains a steps section which has at least one command to implement the purpose of the stage. Here is my example:
```
pipeline {
    agent {
        ...
    }
    stages {
        stage('checkout') {
            steps {
                checkout([
                    $class: 'GitSCM', 
                    branches: [[name: '*/master']],
                    userRemoteConfigs: [
                        [
                            url: '[Git_URL]',
                            credentialsId: 'gitlab'
                        ]
                    ],
                    extensions:[[$class: 'CleanBeforeCheckout']]
                ])
            }
        }
        stage('build') {
            steps {
                sh 'mvn compile'
            }
        }
        stage('test') {
            steps {
                sh 'mvn test'
            }
        }
    }
}
```
* There are stages: 'checkout', 'build' and 'test'. They are just a name to indicate what is going to do in each stage.
*  In ['checkout'](https://www.jenkins.io/doc/pipeline/steps/workflow-scm-step/#checkout-general-scm) stage, there is a special step that allows to run checkouts using any configuration options offered by any Pipeline-compatible SCM plugin. A basic 'GitSCM' class object is used for pulling a project from any SCM using Git. Also, you have to tell which branch to be pulled, the project's URL and a credential id (for private projects). Credential ID can be created in 'Manage Credential' under 'Manage Jenkins'. This [link](https://www.jenkins.io/doc/book/using/using-credentials/#adding-new-global-credentials) is for more details. I found 'GitSCM' is only work with 'Username and password' credential type but not 'Secret Text' type (i.e. a token). 'GitSCM' provides extra features which can be declared in the 'extensions'. Here, I want to clean up the workspace before checking out the source codes.
*  In 'build' stage, it is just to execute a Maven compile command `mvn compile`.
*  In 'test' stage, it performs all tests to the project by a Maven command `mvn test`.

## Jenkins Further Configuration for Docker Integration
Jenkins is running in a container in the Docker. It cannot communicate with the Docker host by default. Jenkins has to mount to /var/run/docker.sock file. Once mounted, Jenkins can run docker command to tell the Docker creating a new container and removing it when finish. Moreover, Jenkins need to mount to /usr/bin/docker for using docker program. So, here are two volumes added to the docker-compose file.
```
    volumes: 
      - /var/run/docker.sock:/var/run/docker.sock 
      - /usr/bin/docker:/usr/bin/docker 
      - ...
```
Be careful the permission of dock.sock, it only allows root and members of docker group to access. In Jenkins, jenkins account is used to run the application. So it cannot read the docker.sock file. That's why a custom Jenkins image is needed. Here is an example:
```
FROM jenkins/jenkins:lts-jdk11 

USER root 

RUN groupadd -g 1001 docker 

RUN usermod -aG docker jenkins 
```
* Create a docker group with gid 1001. The gid must as same as the gid of the host's docker group. Run `cat /etc/group | grep docker` to find out.
* Add jenkins account to docker group. Then jenkins can access docker.sock and use docker command without problem.
* Of course, remember to change image to this new build image in the start-up script or docker-compose file.