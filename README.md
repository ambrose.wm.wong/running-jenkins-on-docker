# Running Jenkins on Docker

## Prerequisites
### Docker installed
One advantage of running Jenkins on Docker is almost OS independent. If you install a docker desktop on Windows with WLS 2, the Jenkins installation is as same as Linux installation.
## Jenkins installation
In official document, it recommends to use [jenkinsci/blueocean image](https://hub.docker.com/r/jenkinsci/blueocean/). As a Java developer and a Jenkins beginner, I want to use jdk-11 in my projects and CI/CD processes. Therefore I use another image [jenkins/jenkins:lts-jdk11](https://hub.docker.com/layers/jenkins/jenkins/lts-jdk11/images/sha256-dfe766e539dc2f47600229c7ed5bb7950b34d2c61f1ae240ef9393cbc0df6e68?context=explore). 

In jenkins container, 'jenkins' account (UID 1000) is used to run the application. Therefore, those mapped volumes must grant read/write rights to UID 1000 (i.e. 'jenkins'). You can change the ownership of the volumes or grant right/write to others group. For security reason, I prefer to the first option.

### Configuration
* Create the following folder structure.
```  
  |
  `---[jenkins]
          |---[backup]  // for backup
          |---[data]    // JENKINS_HOME
          `---[config]  // place jenkins' docker-compose file
```
* In Linux, change the ownership of those folders.
  * Check your account's UID: `id [your ID]`
  * If your UID is 1000, then do nothing on the ownership.
  * Otherwise, change the ownership to 1000: `sudo chown -R 1000:1000 jenkins`
* In Windows, do nothing on the ownership.
### Start up and running
* There are two ways to start up Jenkins.
  * Use `docker run` command  
    ```
    docker run --restart=always -d \
       -p 8080:8080 -p 50000:50000 \
       -u jenkins \
       -v $PWD/jenkins/data:/var/jenkins_home \
       -v $PWD/jenkins/backup:/var/lib/jenkins/backup \
       --name jenkins -t jenkins/jenkins:lts-jdk11
    ```
  * Use docker-compose file
    ```
    version: '3.8' 

    services: 
      jenkins: 
        image: jenkins/jenkins:lts-jdk11 
        user: jenkins 
        restart: always 
        ports: 
          - 8080:8080 
          - 50000:50000 
        container_name: jenkins 
        volumes: 
          - /[path]/jenkins/data:/var/jenkins_home 
          - /[path]/jenkins/backup:/var/lib/jenkins/backup
    ```
    then run `docker-compose -f $PWD/jenkins/config/docker-compose.yml up -d`


## CI/CD implementations in Jenkins.
### Run CI/CD just inside the master node
It is the simplest way to have fun. You don't have to worry too much about the configuration and the access rights. One thing you have to do is to build a customized Jenkins docker image. Because all available official images just provide the tools for up and running Jenkins and they don't have extra tools to preform CI/CD processes you want, say compiling sources, doing test cases, and deploying. 

In my case, I use maven to build, test and deploy my Java projects. Actually, Jenkins can install Maven directly in 'Global Tool Configuration'. However, I think it is a good chance to learn how to build your own docker image. [This link has the details.](https://gitlab.com/ambrose.wm.wong/running-jenkins-on-docker/-/tree/master/Run%20CI-CD%20just%20inside%20the%20master%20node#build-a-all-in-one-jenkins-docker-image)

### Run CI/CD on another containers in the same Docker (dynamically)
Jenkins can activated other agents (slaves) in other servers to run CI/CD jobs remotely. Due to limited resources, it is not easy to setup a multiple servers environment (physical or VM) for an experimental purpose. But we can use Docker to simulate this kind of environment by running several containers with lesser resources.

Usually, agents can already be installed and wait for Jenkins calls. In this section, however, it shows how Jenkins creates an agent (a container in the same Docker) when a job is starting. The tasks are performed inside the container. When the job finishes, the container will be removed automatically.

Before starting a job, Jenkins submits a docker command to create a container and mount with the job's workspace under JENKINS_HOME/workspace (i.e. jenkins/data/workspace in my case) which is the working directory of the job. Then the agent can access the folder directly. Each job has its own workspace. The source codes are cloned there and the result can also be found there.

### Run CI/CD on a remote Docker
It is a more sophisticated way to run jobs on the other dockers. The idea is to share the work load of Jenkins to other machines. Docker engine API is used to establish communication between Jenkins and Docker. Moreover, there are two specific agents needed for this purpose: jnlp and ssh agents. They use different protocols to communicate with Jenkins. Jnlp uses Java Web Start and ssh uses SSH. In fact, in doing so, there are a lot of configurations to set as well. 

Due to limited resources, it may hard to try running a job on another machine. This situation can be simulated on the same Docker.

  
  
  
Why? User account and access rights of volumes are the problems. The user account running Jenkins on Docker is 'jenkins'. It doesn't exist in the host OS. Also, in common way, Jenkins will mount to the host file system, say mounting to your home sub-folder '$HOME/jenkkins'. For security reason, the owner of mounted volumes has to change to 'jenkins', rather than granting the volumes for everyone to access.

Here is the steps:
* (optional) start Jenkins by using docker run command, usually the uid of 'jenkins' is 1000.
 
